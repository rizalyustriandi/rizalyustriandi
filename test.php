<!DOCTYPE html>
<html>

    <head>
        <title>Belajar PHP</title>
        <!-- <link rel="stylesheet" href="style.css"> -->
    </head>

    <body>
        <?php
            echo "<h1>BELAJAR PHP </h1>";
            echo "saya sedang belajar PHP<br>";
            print print ('IPB');
            echo "<br>IPB";
            echo '<br>satu', ' dua', ' tiga';

            //variabel
            $makanan = "RUJAK";
            $x = 4;
            $y = 5;
            $z = "a";

            echo "<br>Makanan kesukaan saya adalah ", $makanan;
            echo "<br>penambahan ", $x + $y;
            
            //conditional
            $bil= 1 ;
            echo "<br>bil = 1";

            if ($bil % 2 == 0){
                echo "<br>bilangan itu genap";
            } else{
                echo "<br>bilangan itu ganjil";
            }

            $bilangan= 2;
            echo "<br>bilangan = 2";
            switch($bilangan % 2){

                case 1 :
                    echo "<br>Ganjil";
                    break;

                default :
                    echo "<br>Genap";
            }

            //Loop
            for ($i=1; $i <=3; $i++){
                echo "<br>".$i;
            }

            $j = 1;
            while ($j <= 3){
                echo "<br".$j++;
            }

            $k = 1;
            do {
                echo '<br>'.$k++;
            } while ($k <= 3);

            $data = ['satu', 'dua', 'tiga'];
            foreach($data as $i) {
                echo '<br>'.$i;
            }

            
        ?>
    </body>
</html>